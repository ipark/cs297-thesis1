\documentclass[conference,11pt]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnot. 
% If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings} % needed for the inclusion of source code

\usepackage{mips}
\usepackage[capitalise]{cleveref}

\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}
\makeatother

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\renewcommand{\ttdefault}{cmtt}
\renewcommand\lstlistingname{\textsc{Code Snippet}}
\crefname{listing}{\textsc{Code Snippet}}{\textsc{Code Snippet}}
\renewcommand\figurename{\textsc{Figure}}
\crefname{figure}{\textsc{Figure}}{\textsc{Figures}} 
\crefname{table}{\textsc{Table}}{\textsc{Tables}} 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\LARGE\textbf{CS297: Adversarial Attack in Textual Deep Neural Network}}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{\IEEEauthorblockN{Inhee Park \texttt{(ipark.c@gmail.com)}}
	\IEEEauthorblockA{\textit{Department of Computer Science},
		\textit{San Jose State University}, San Jose, CA, USA}   }

\maketitle
\thispagestyle{plain}
\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ABSTRACT %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{abstract}

%GitLab repository: \texttt{https://gitlab.com/ipark/cs297}
%\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEY WORDS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{IEEEkeywords}
%Adversarial Attacks/Examples,
%Natural Language Processing (NLP), 
%Textual Deep Neural Network
%\end{IEEEkeywords}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% INTRODUCTION %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{\#1. Weekly Report -- 01/27/2020}}
\subsection*{\textsc{\bfseries{Summary This Week}}}
High level summary of a possible thesis topic would be how to attack deep neural networks (DNN) of NLP models by perturbing its robustness in prediction with adversarial example generation.  Conversely, to defend such adversarial attacks by preserving robustness of the DNNs of NLP, should understand types of threats and underlying NLP architectures. 
Thus I read the survey article on ``Adversarial Attacks on Deep Learning Models in Natural Language Processing: A Survey'' \cite{AASurvey19a}.

\subsection*{\textsc{\bfseries{To-Do Next Week}}}
\begin{enumerate}
\item 
More detailed understanding on the defence method of adversarial attack;
\item 
Select a couple of white-box and black-box attack methods and review them in detail. 
\item 
Another reading on  ``The Threat of Adversarial Attacks on Machine Learning in Network Security -- A Survey'' \cite{AASurvey19b} to define research scope.
	\item 
\end{enumerate}

\subsection*{\textsc{\bfseries{Definition of Adversarial ``Attack'' (= Adversarial Example Generation)}}}
Ideal deep neural networks (DNN) should predict correct class $\mathbf{y}$ to the perturbed input $\mathbf{x'}$ with perturbation term  $\eta$ aiming to fool the DNN: 
\[ f(\mathbf{x'}) = \mathbf{y}, \;\;\;(\mathbf{x'} = \mathbf{x}+\eta).\]
In contrast, the victim DNN would have high confidence on wrong prediction $\mathbf{y'}$:
\[ f(\mathbf{x'}) = \mathbf{y'}, \;\;\;(\mathbf{y'} \ne \mathbf{y}).\]
Thus, the goal of adversarial attack (by generating adversarial examples, $\mathbf{x'}$) is enforcing the DNN to incorrect label prediction
\[ f(\mathbf{x'}) \ne \mathbf{y}.\]

\subsection*{\textsc{\bfseries{Categories of Adversarial Attacks: White-box, Black-box}}}
\cref{f:summaryModels} provides a high-level overview of adversarial attack models: by model access (white-box vs black-box), by granularity (character, word, sentence-level) and by architectures of NLP, etc.
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/summaryModels.png}
	\caption{Categories of adversarial attack models on textual DNN}
	\label{f:summaryModels}
\end{figure} 
\cref{f:whitebox} summarizes \textbf{white-box attack}. It requires  models' full information (architecture, parameters, loss functions, activation functions, input and output data), then approximates and incorporates the worst-case perturbation. 
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/whitebox.png}
	\caption{White-box attack methods. PE: Portable Executable; TC: Text Classification; SA: Sentiment Analysis; TS: Text Summarisation; MT: Machine Translation; MAD: MAalware Detection; MSP: Medical Status Prediction; MRC: Machine Reading Comprehension; QA: Question Answering; WMD: Word Mover's Distance; '–': not available.}
	\label{f:whitebox}
\end{figure} 
\cref{f:blackbox} summarizes \textbf{Black-box attack} does not require the details of the model, just need to access the input and output relying on heuristics to generate adversarial examples.
I will select some of white-box and black-box attack methods to focus on to formulate detailed research methods.
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/blackbox.png}
	\caption{Black-box attack methods. MRC: Machine Reading Comprehension; QA: Question Answering; VQA: Visual Question Answering; DA: Dialogue Generation; TC: Text Classification; MT: Machine Translation; SA: Sentiment Analysis; NLI: Natural Language Inference; TE: Textual Entailment; MD: Malware Detection. EdDist: Edit Distance of text, JSC: Jaccard Similarity Coefficient, EuDistV: Euclidean Distance on word Vector, CSE: Cosine Similarity on word Embedding. '-': not available.
}
	\label{f:blackbox}
\end{figure} 

\subsection*{\textsc{\bfseries{Benchmark Dataset for Adversarial Attacks}}}
\cref{f:benchmark} listed available benchmarking datasets that I can use for the project.  I have worked on the classification category on text classification (CS274 project) and sentiment analysis (CS271 project), but I will consider to explore other categories as well. 
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/benchmarkDatasets.png}
	\caption{Attacked applications and benchmark datasets}
	\label{f:benchmark}
\end{figure} 


\subsection*{\textsc{\bfseries{Defence of Adversarial Attacks}}}
The primary purpose of generating adversarial examples is to enhance the DNN models' robustness. (1) Adversarial training incorporates adversarial examples in the model training process; (2) Knowledge distillation manipulates the neural network model and trains a new model.


%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\textsc{\bfseries{Motivation: Hard to capture semantic similarity for the ``low lexical'' sentences; could be improved by adversarial examples}}}
On the other hand, there is a benefit of generating adversarial examples (black-box attack) which provides an enhanced learning for the DNN models of NLP. This is an excerpt from my CS274 project (Repo: https://gitlab.com/ipark/cs274-wi). 
%
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/sentenceSemantics.png}
	\caption{Result on the sentence-level semantic similarity}
	\label{f:sentenceSemantics}
\end{figure} 
%
Sentence level semantics predicts opposite to the expectation as shown in \cref{f:sentenceSemantics}.  Expectation was to embeddings can capture paraphrase relation with higher similarity score in spite of low lexical pair of sentences (highlighted with light yellow color).  But, actual similarity outcome is the higher the similarity score for more lexical matches.  Such an opposite prediction pattern is consistent regardless of embeddings and similarity metrics. Yet, in spite of advanced NLP models, as experimented in this project, lexically similar sentences show higher similarity than semantically similar sentences.  We note that other NLP researchers also had similar problems; as a solution, they constructed enhanced Quora Question dataset by scrambling the word inside original dataset (called `Paraphrase Adversaries from Word Scrambling') and re-train the BERT model with PAWS dataset.  And they demonstrated more correct Semantic Textual Similarity results as shown in \cref{f:PAWS}.
%
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/PAWSset.png}
	\includegraphics[width=\columnwidth]{./Figs/PAWSbert.png}
	\caption{(top) Paraphrase Adversaries from Word Scrambling; (bottom) improved STS results. Reference: \cite{PAWS}}
	\label{f:PAWS}
\end{figure} 
%

I found that identifying the pair of sentences with low lexical match as paraphrase is a known issue regarding the Quora Question Pair dataset.  To improve prediction for such challenging cases, researchers construct an enhanced dataset, called Paraphrase \textbf{Adversaries} from Word Scrambling (PAWS) by scrambling words to generate more dataset based on the Quora dataset.  Then as shown in the \cref{f:PAWS}, train this enhanced PAWS dataset using the same base architecture of BERT, those challenging paraphrase sentence pairs were correctly predicted (compare the columns Gold as Ground Truth vs. BERT+PAWS).


%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}
\bibitem{AASurvey19a}	
Adversarial Attacks on Deep Learning Models in Natural Language Processing: A Survey;
Wei Emma Zhang, Quan Z. Sheng, Ahoud Alhazmi, Chenliang Li;
arXiv:1901.06796 [cs.CL]

\bibitem{AASurvey19b}
The Threat of Adversarial Attacks on Machine Learning in Network Security -- A Survey;
Olakunle Ibitoye and Rana Abou-Khamis and Ashraf Matrawy and M. Omair Shafiq; arXiv:1911.02621 [cs.CR]

\bibitem{PAWS}
Paraphrase Adversaries from Word Scrambling;
Yuan Zhang, Jason Baldridge, Luheng He;  arXiv:1904.01130 [cs.CL]

	


\end{thebibliography}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}
