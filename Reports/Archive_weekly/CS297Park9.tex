\documentclass[conference,11pt]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnot. 
% If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings} % needed for the inclusion of source code

\usepackage{mips}
\usepackage[capitalise]{cleveref}

\usepackage{listings}             % Include the listings-package
\definecolor{javared}{rgb}{0.6,0,0} % for strings
\definecolor{javagreen}{rgb}{0.25,0.5,0.35} % comments
\definecolor{javapurple}{rgb}{0.5,0,0.35} % keywords
\definecolor{javadocblue}{rgb}{0.25,0.35,0.75} % javadoc
\lstset{language=Python,
basicstyle=\footnotesize\linespread{0.8}\ttfamily,
keywordstyle=\color{javapurple}\bfseries,
stringstyle=\color{javared},
commentstyle=\color{javagreen},
morecomment=[s][\color{javadocblue}]{/**}{*/},
morecomment=[s][\color{javadocblue}]{/*}{*/},
%numbers=left,
%numberstyle=\ttfamily\tiny\color{black},
%stepnumber=1,
showspaces=false,
showstringspaces=false,
morekeywords={List, String, Integer, Double, LinkedList},
}


\makeatletter
\def\lst@makecaption{%
	\def\@captype{table}%
	\@makecaption
}
\makeatother

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\renewcommand{\ttdefault}{cmtt}
\renewcommand\lstlistingname{\textsc{Code Snippet}}
\crefname{listing}{\textsc{Code Snippet}}{\textsc{Code Snippet}}
\renewcommand\figurename{\textsc{Figure}}
\crefname{figure}{\textsc{Figure}}{\textsc{Figures}} 
\crefname{table}{\textsc{Table}}{\textsc{Tables}} 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BEGIN DOCUMENT %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TITLE %%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{\LARGE\textbf{CS297: Deep Reinforcement Learning}\\
{\Large Final Project: Malware Generation/Detection using Reinforcement Learning}}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AUTHOR %%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\author{\IEEEauthorblockN{Inhee Park \texttt{(ipark.c@gmail.com)}}
\IEEEauthorblockA{\textit{Department of Computer Science},
\textit{San Jose State University}, San Jose, CA, USA}   }


\maketitle
\thispagestyle{plain}
\pagestyle{plain}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ABSTRACT %%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{abstract}

%GitLab repository: \texttt{https://gitlab.com/ipark/cs297}
%\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% KEY WORDS %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{IEEEkeywords}
%Adversarial Attacks/Examples,
%Natural Language Processing (NLP), 
%Textual Deep Neural Network
%\end{IEEEkeywords}
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUMMARY THIS WEEK %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{\large\textbf{\#9. Weekly Report -- \makeatletter \today \makeatother}}
\vspace{10pt}
\subsection*{\large\textsc{\bfseries{Summary This Week}}}
\begin{itemize}
    \item Start to look into ``Deep'' part of Deep Reinforcement Learning (DRL) in terms of
        characteristics of feedback to learn.  This comprehension is critical to setup the DRL.
     \item Survey what/how to select/setup the DRL with a concrete example of Pole-Balancing problem:
         1) value function to approximate, 2) what to optimize in loss function, 3) 
         neural network architecture, 4) TD target, 5) exploitation/exploration method etc.
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\large\textsc{\bfseries{Feedback that Deep RL to learn}}}
\begin{itemize}
\item {\textbf{Deep}} of DRL refers to non-linear function approximation. 
\item Characteristics of feedback that DRL's agent to learn from response of environment are
    simultaneously 1) sequential; 2) evaluative; and 3) sampled.
\end{itemize}
%
\subsection*{\large\textsc{\bfseries{Sequential feedback}}}
\begin{itemize}
    \item \textbf{Sequential feedback $\leftrightarrow$ One-shot feedback.}
\item (\cref{f:seq} top) One-shot feedback is supervised learning where the model
    learn from the given target/label toward reducing an error.
\item (\cref{f:seq} bottom) Sequential feedback should be balanced
    between \textbf{immediate reward vs. long-term reward} to find optimal actions
        using discounted factor $\gamma$.
\begin{figure}[th!]
	\centering
	\includegraphics[width=0.7\columnwidth]{./Figs/super.png}
	\includegraphics[width=0.7\columnwidth]{./Figs/seq.png}
    \caption{One-shot (up) vs. Sequential Feedback (bottom)}
	\label{f:seq}
\end{figure} 
\end{itemize}
%
\subsection*{\large\textsc{\bfseries{Evaluative feedback}}}
\begin{itemize}
    \item  \textbf{Evaluative feedback $\leftrightarrow$ Supervised feedback.}
\item (\cref{f:eval} left) While Supervised feedback is where there is a target
to decide immediately whether the current estimate is good enough w/r/t target or not. 
\item Or in RL Supervised feedback is from POMDP where transition function
    and reward is prescribed for each state or state-action pair, thus
        no uncertainty.
\item (\cref{f:eval} right) Evaluative feedback doesn't have prior reward information to compare
    with the current reward, thus uncertainty involved. 
        Therefore, should have some target to be used as a reference (like target label in supervised learning). 
\item In RL, temporal difference target is such a reference.
    \texttt{td\_target = reward + gamma * Q[s'].max()}, where \texttt{s'} is next state.
\item Here to obtain a \texttt{Q[s'].max()} where no prior information is available,
    hence we should consider whether to acquire new knowledge (exploration by random action selection) or to utilize prior knowledge (exploitation by greedy action selection, best at that moment).
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/eval.png}
    \caption{Supervised (left) vs. Evaluative Feedback (right)}
	\label{f:eval}
\end{figure} 
\item \textbf{Exploration-Exploitation} Methods:
    \begin{enumerate}
        \item Greedy: exploitation all the time
        \item Random: exploration all the time
        \item $\epsilon$-greedy: let $\epsilon=0.01$ (mostly exploitation)\\
            \texttt{if random()} $>\epsilon$:\\
            \texttt{action=argmax(Q) \# exploitation}\\
            \texttt{else:} \\
            \texttt{action=random(len(Q)) \# exploration}
        \item Decaying $\epsilon$-greedy: (early exploration, then exploitation)
        \item Softmax: select action randomly but in proportion to the estimate
        \item Frequentest based sampling:  select the most frequently taken actions with 
            addition of some uncertainty factor
        \item Thomson Sampling: balancing between reward vs risk using Gaussian distribution. Taking action not only
            based on the highest probability, but also based on the low uncertainty.
    \end{enumerate}
\end{itemize}
%
\subsection*{\large\textsc{\bfseries{Sampled feedback}}}
\begin{itemize}
    \item \textbf{Sampled feedback $\leftrightarrow$ Tabular/Exhaustive feedback.}
\item Curse of dimensionality problem comes either from (1) discrete but high-dimensional 
    state space; or (2) relatively low-dimensional but continuous state space;
to enumerate exhaustive state is infeasible. 
\item Instead of explicitly update the value-function at each state of state-action pair visited, 
    update the value-function as a whole by \textbf{non-linear function approximator using neural network = Deep RL.}
\item {\color{red}I am not yet clear this ``sampled'' feedback means to generate sample then train with it OR just mean to train using already-generated data from other source.}
\end{itemize}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\large\textsc{\bfseries{DRL Setup for Pole-Balancing Problem}}}
\begin{itemize}
%1
\item Approximate the action-value function $Q(s,a; \theta)$ ($\theta$ is a weight of a layer)
    to optimize $q*(s,a)$, which finds the optimal policy. The following is a loss function:
        \[L_i(\theta_i) = E_{s,a}\left[(q*(s,a)\ - Q(s,a;\theta_i))^2\right]\]
%2
\item Neural Network architecture -- \cref{f:nns} for example,
\begin{enumerate}
    \item Input-layer(states) and output-layer(possible actions)
    \item Input-layer(states with a specific action) and output-layer(optimal one action)
\end{enumerate}
\begin{figure}[th!]
	\centering
	\includegraphics[width=\columnwidth]{./Figs/nn2.png}
	\includegraphics[width=\columnwidth]{./Figs/nn1.png}
    \caption{State-action-in-value-out architecture (top); 
    State-in-values-out architecture (bottom)}
	\label{f:nns}
\end{figure} 
\item Use off-policy TD targets $(r + \gamma \max_a (Qs', a'; \theta))$ to evaluate policies.
\item Use $\epsilon$-greedy strategy to improve policies.
\item Use mean squared error (MSE) for our loss function $L(\theta)$.
\item Selecting the optimization method (e.g. RMSprop) with learning rate(0.0005).
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\large\textsc{\bfseries{Survey of DRL Methods}}}
\begin{itemize}
\item NFQ (Neural Fitted Q)
\item DQN (Deep Q-Network)
\item Actor-Critic 
\end{itemize}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
%% SUMMARY THIS WEEK %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection*{\large\textsc{\bfseries{To-Do Next Week}}}
\begin{itemize}
    \item Consider \textbf{Pole-Balancing} problem (\cref{f:cartpole}), which is low-dimensional but\textbf{continuous} state space system to train in DRL using OpenAI with PyTorch.
\item Figure out how to train \textbf{discrete} and high dimensional state space system (8x8 Frozen Lake) using DRL.
\end{itemize}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{onecolumn}
\subsection*{\large\textsc{\bfseries{Pole-Balancing Problem}}}
\begin{itemize}
\item \textbf{Problem Statement} ---
\begin{figure}[th!]
    \centering
\includegraphics[width=0.75\columnwidth]{./Figs/cartpole.png}
\caption{Pole-Balancing or Cart-Pole Problem (Ref: \cite{RLbook})}
\label{f:cartpole}
\end{figure} 
\item \textbf{Episodic=Discrete Case}
    \begin{itemize}
    \item Reward ($R_t$) = penalty/credit that agents receives from environment after taking an action.
    \item Return ($G_t$) = Cumulative Reward.
    \item Expected Return ($G_t$) = reward sequence based on the current Rt:  
        \[G_t=R_{t+1}+R_{t+2}+\cdots+R_T\]
    \item Agent's Goal = maximize Expected Return in the long run
    \item Reward is +1 at every episode t;
    \item Terminal State Reward = 0 at T, thus
    \item Return is $G_t=\sum_{k=0}^{T=\infty} = \infty$
    \end{itemize}
\item \textbf{Continuing Case}
    \begin{itemize}
        \item Continuing tasks = In many cases the agent–environment interaction does not break into identifiable ``episodes'', but goes on continually without limit.
        \item Discounted Return ($G_t$) = reward sequence based on the current Rt with discounting factor ($0 \le \gamma \le 1$)
            \[G_t=R_{t+1}+\gamma R_{t+2}+\gamma^2 R{t+3} + \cdots + \gamma^{T-1} R_T\]
        \item Agent's Goal = maximize Discounted Return in the long run
        \item Reward is 0 at every time steps t;
        \item Terminal State Reward = -1 at T, thus
        \item Return is $G_t=\gamma^0\cdot 0+\gamma^1\cdot 0 + \gamma^2 \cdot 0 + \cdots + \gamma^{T-1}\cdot(-1)$
        \item The return at each time would then be related to $-\gamma^K$, where $K$ is the number of time steps before failure. The return is maximized by keeping the pole balanced for as long as possible.
        \item I initially thought that pole balancing is ``continuous'' because observation (=state) is defined multiple variables (cart position, cart velocity, cart angles), moreover, each variable has a range \texttt{[-float\#, +float\#]}, thus states are in ``continuous'' (too many to define) space.
    \end{itemize}
\item \textbf{Observation} --- Ref: \cite{github-cartpole}
\begin{verbatim}
Num	Observation                 Min         Max
0	Cart Position             -4.8            4.8
1	Cart Velocity             -Inf            Inf
2	Pole Angle                 -24 deg        24 deg
3	Pole Velocity At Tip      -Inf            Inf
\end{verbatim}
\item \textbf{Actions} --- 
\begin{verbatim}
Num	Action
0	Push cart to the left
1	Push cart to the right
\end{verbatim}
\item \textbf{Reward} ---\\
1 for every step taken, including the termination step
\item \textbf{Starting State} ---\\
    All observations are assigned a uniform random value in \texttt{[-0.05..0.05]}
\item \textbf{Episode Termination} ---
    \begin{enumerate}
    \item Pole Angle is more than 12 degrees
    \item Cart Position is more than 2.4 (center of the cart reaches the edge of the display)
    \item Episode length is greater than 200
    \item Considered ``solved'' when the average reward is greater than or equal to 195.0 over 100 consecutive trials.
    \end{enumerate}
    \end{itemize}
\end{onecolumn}

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{figure}[th!]
%	\centering
%	\includegraphics[width=\columnwidth]{./Figs/}
%	\caption{}
%	\label{f:}
%\end{figure} 

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BIBLOIGRAPHY %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}
\bibitem{RLbook}
Reinforcement Learning: An Introduction
R. Sutton, and A. Barto.
The MIT Press, Second edition, (2018)
\bibitem{github-cartpole}
    \texttt{https://github.com/openai/gym/blob/master/gym/envs/classic\_control/cartpole.py}
\bibitem{DRLbook2020}
    ``Grokking Deep Reinforcement Learning'', Miguel Morales,
        Manning Publication (MEAP)  Publication in April 2020 (estimated),
ISBN 9781617295454  
\end{thebibliography}

\end{document}
